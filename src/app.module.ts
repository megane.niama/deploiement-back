import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {MessageEntity} from "./entity/message.entity";

@Module({
  imports: [ConfigModule.forRoot(),
  TypeOrmModule.forRoot({
    type: 'mysql',
    host: process.env.REACT_APP_BDD_HOST,
    port: +process.env.REACT_APP_BDD_PORT,
    username: process.env.REACT_APP_BDD_USERNAME,
    password: process.env.REACT_APP_BDD_PASSWORD,
    database: process.env.REACT_APP_BDD_NAME,
    entities: [MessageEntity],
    synchronize: true,
  }),TypeOrmModule.forFeature([MessageEntity])],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
